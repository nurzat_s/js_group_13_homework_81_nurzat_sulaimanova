export class Link {
  constructor(
    public id: string,
    public URL: string
  ) {}
}

export interface LinkData {
  [key: string]: any;
  URl: string
}
