import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Link, LinkData } from '../models/link.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  getLinks() {
    return this.http.get<Link[]>(environment.apiUrl + '/links').pipe(
      map(response => {
        return response.map(linkData => {
          return new Link(
            linkData.id,
            linkData.URL
          )
        })
      }));
  }

  createLink(linkData: LinkData) {
    const formData = new FormData();

    Object.keys(linkData).forEach(key => {
      if(linkData[key] !== null) {
        formData.append(key, linkData[key]);
      }
    });
    return this.http.post(environment.apiUrl + '/links', formData);
  }
}
