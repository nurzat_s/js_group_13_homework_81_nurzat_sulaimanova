const express = require('express');
const { nanoid } = require('nanoid');
const config = require('../config');
const Link = require('../models/Link');


const router = express.Router();

router.get('/', async (req, res, next) => {
 try {
   const link = {};
   const sort = {};

   let links = await Link.find(link).sort(sort);

   return res.send(links)
 } catch (e) {
   next(e);
 }
});

router.get('/:shortUrl', async (req, res, next) => {
  try {
    const link = await Link.findOne({shortUrl: req.params.shortUrl});

    if (!link) {
      return  res.status(404).send({message: 'Not found'});
    }
    return res.status(301).redirect(req.params.originalUrl);
  } catch (e) {
    next(e);
  }
});


router.post('/',  async (req, res, next) => {
  try {
    let link = req.body;
    const links = new Link(link);

    await links.save();

    return res.send({message: 'Created new link', id: link._id});
  } catch (e) {
    next(e);
  }

});

module.exports = router;